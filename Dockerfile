FROM python:3.9-alpine

COPY app.py .
COPY list.json .

CMD ["python3", "app.py"]