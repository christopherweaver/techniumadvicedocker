import json
import random

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

if __name__ == "__main__":
    with open("list.json", "r") as file:
        json_list = json.load(file)
    print("\n")
    print(f"{bcolors.OKCYAN}{json_list[random.randint(1, len(json_list))]}{bcolors.ENDC}")
    print("\n")
